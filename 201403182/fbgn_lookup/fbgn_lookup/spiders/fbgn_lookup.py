#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   FBGN Lookup

   :author: Sam Hart <hartsn@gmail.com>
"""

from scrapy.spider import Spider
from scrapy.selector import Selector

class FbgnSpider(Spider):
    name = 'fbgn'

    def __init__(self, fbgn_id=None, *args, **kwargs):
        super(FbgnSpider, self).__init__(*args, **kwargs)
        allowed_domains = ["flybase.org"]
        self.start_urls = ['http://flybase.org/reports/%s.html' % fbgn_id]

    def parse(self, response):
        sel = Selector(response)
        cgnum = sel.xpath("//tr[th//text()[contains(., 'Annotation symbol')]]/td//text()").extract()[1]
        name = sel.xpath("//tr[th//text()[contains(., 'Annotation symbol')]]/td//text()").extract()[0]
        aka = sel.xpath("//tr[th//text()[contains(., 'Also Known As')]]/td//text()").extract()[0].split(',')

        print cgnum
        print name
        print aka