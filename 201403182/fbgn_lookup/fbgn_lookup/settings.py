# Scrapy settings for fbgn_lookup project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'fbgn_lookup'

SPIDER_MODULES = ['fbgn_lookup.spiders']
NEWSPIDER_MODULE = 'fbgn_lookup.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'fbgn_lookup (+http://www.yourdomain.com)'
