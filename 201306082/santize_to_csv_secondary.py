#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Vain attempt at ANOTHER secondary screen metadata sanitation tool

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

from __future__ import print_function
import os.path
from types import StringTypes
from copy import copy

from sanitizebase import Sanity
from dfreader import get_reader, Cell

class SecondarySanity(Sanity):

    def _initialize_pre(self):
        self._epilog = 'fbgn_annotation, see http://flybase.org/static_pages/docs/datafiles.html#annotation_ids. ' + \
                   'fb_synonym, see http://flybase.org/static_pages/docs/datafiles.html#flybase_synonyms.'
        self._description = 'Metadata sanitizer'
        self._name = '201306082 secondary'

    def _initialize_parser(self):
        self.parser.add_argument('primary_file', type=str,
        help='The primary screen metadata CSV from earlier sanitizer')
        self.parser.add_argument('status_file', type=str,
        help='The gene model status file (expected to be CSV, first field FBgn, last field status)')

    def _initialize_final(self):
        gene_status_file = os.path.abspath(self.args.status_file)
        primary_file = os.path.abspath(self.args.primary_file)

        self.status_lookup = dict()
        self.primary_lookup = dict()

        genecsv = get_reader('csv')
        print('------------------------------------')
        print('CACHING GENE STATUS FILE')
        print(' > (FBgn) : (STATUS)')
        for row in genecsv(gene_status_file):
            if len(row) > 0:
                self.status_lookup[row[0].value] = row[-1].value
                print(" > (%s) : (%s)" % (row[0].value, row[-1].value))

        primary_reader = get_reader('csv')
        print('------------------------------------')
        print('CACHING PRIMARY METADATA FILE')
        print(' > (CG number) : (SEVERITY)')
        isHeader = True
        for row in primary_reader(primary_file):
            if isHeader:
                isHeader = False
            else:
                cgnum = 'CG%s' % str(row[4])
                self.primary_lookup[cgnum] = {
                    'Adhesion_Defect_Primary': row[2],
                    'Gene_Name': row[6],
                    'Gene_Symbol': row[7],
                    'Gene_Model_Status': row[10],
                }
                print(" > (%s) : (%s)" % (cgnum, row[2]))

    def _transform_header(self):
        line = [
            "Plate",
            "Well",
            "Adhesion_Defect_Secondary",
            "Adhesion_Defect_Primary",
            "Adhesion_Defect_Secondary_Average",
            "CG_Number",
            "Nearest_Human_Homologue",
            "Regulatory_Hub",
            "RNAi_Primer_1",
            "RNAi_Primer_2",
            "Gene_Name",
            "Gene_Symbol"
            ]
        self.header = line
        self.deal_with_line(self.header)

    def _transform_row(self, row):
        #pass
        lines = []
        line1 = []
        line1.append("Secondary %03da" % int(row[0].value))
        line1.append("%s%02d" % (row[1].value, int(row[2].value)))
        def f(x):
            if x is None:
                return u""
            if x.__class__ == Cell:
                x = x.value
            if x.__class__ in StringTypes:
                return x
            return "%.2f" % x
        line1.append(f(row[3].value))
        cgnum = row[6].value
        if self.primary_lookup.has_key(cgnum):
            line1.append(f(self.primary_lookup.get(cgnum)[0]))
        else:
            line1.append(u'')
        for i in row[5:]:
            line1.append(f(i.value))
        if self.primary_lookup.has_key(cgnum):
            line1.append(f(self.primary_lookup.get(cgnum)[1]))
            line1.append(f(self.primary_lookup.get(cgnum)[2]))
        else:
            line1.append(u'')
            line1.append(u'')
        lines.append(line1)
        #import pdb; pdb.set_trace()

        line2 = []
        line2.append("Secondary %03db" % int(row[0].value))
        line2.append("%s%02d" % (row[1].value, int(row[2].value)))
        line2.append(f(row[4].value))
        cgnum = row[6].value
        if self.primary_lookup.has_key(cgnum):
            line2.append(f(self.primary_lookup.get(cgnum)[0]))
        else:
            line2.append(u'')
            self.errors.append('cgnum "%s" not found in primary_lookup' % cgnum)
        for i in row[5:]:
            line2.append(f(i.value))
        if self.primary_lookup.has_key(cgnum):
            line2.append(f(self.primary_lookup.get(cgnum)[1]))
            line2.append(f(self.primary_lookup.get(cgnum)[2]))
        else:
            line2.append(u'')
            line2.append(u'')
        lines.append(line2)

        for l in lines:
            self.deal_with_line(l)

if __name__ == "__main__":
    s = SecondarySanity()
    s.run()
