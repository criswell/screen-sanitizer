#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# MongoDB metadata setup script for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

import pymongo
from bson.objectid import ObjectId
from copy import deepcopy
import sys
import types
NumberTypes = (types.IntType, types.LongType, types.FloatType, types.ComplexType)

connection = pymongo.Connection()
db = connection['201306082']

# Add indexes
db.fbgn_annotation_ID.ensure_index([
    ('annotation_ID', pymongo.ASCENDING),
    ('secondary_annotation_IDs', pymongo.ASCENDING),
    ('gene_symbol', pymongo.ASCENDING)
])
db.Primary_FB_LW.ensure_index([
    ('CG_Number', pymongo.ASCENDING),
    ('Gene_Model_Status', pymongo.ASCENDING)
])
db.Secondary_FB2.ensure_index([
    ('CG_Number', pymongo.ASCENDING),
    ('Gene_Model_Status', pymongo.ASCENDING),
    ('Well', pymongo.ASCENDING),
    ('Score', pymongo.ASCENDING)
])
db.gene_status.ensure_index([
    ('primary_FBgn', pymongo.ASCENDING)
])

def get_fbgn(cgnum, fbgn_link):
    '''
    Obtain the fbgn given a known cgnum. Returns None if fbgn could not be
    found.

    NOTE: Currently this is lazy and will just return the first match in the
    event we have multiples.
    '''
    c = db.fbgn_annotation_ID.find_one({'gene_symbol': cgnum})
    if c is not None:
        return c['primary_FBgn#']

    # Try to get it from secondary
    c = db.fbgn_annotation_ID.find_one({'secondary_annotation_IDs': cgnum})
    if c is not None:
        return c['primary_FBgn#']

    # Alright, if all else fails, try to strip from link
    if fbgn_link is not None:
        link_array = fbgn_link.split('/')
        if len(link_array) > 3:
            link = link_array[-1].split('.')[0]
            if link is not None:
                return link

    return None

# Make the following elements of fbgn_annotation_ID lists:
#  * secondary_FBgn#s
#  * secondary_annotation_IDs
print ">> Fixing fbgn_annotation_ID322232\n"
for doc in db.fbgn_annotation_ID.find(snapshot=True):
    # Snapshot mode ensures that we are operating on a semi-immutable
    # cursor.
    print doc.get('gene_symbol'),
    for key in ('secondary_FBgn#s', 'secondary_annotation_IDs'):
        try:
            value = doc.get(key)
            if isinstance(value, basestring):
                doc[key] = value.split(',')
                db.fbgn_annotation_ID.save(doc)
        except:
            print doc
            raise

# Fix CG_Number, In_Secondary_Screen in Primary_FB_LW, and obtain the gene
# model status
print "\n\n>> Fixing Primary_FB_LW\n"
for doc in db.Primary_FB_LW.find(snapshot=True):
    # Snapshot mode ensures that we are operating on a semi-immutable
    # cursor.
    print doc.get('CG_Number'),
    try:
        value = doc.get('CG_Number')
        cgnum = None
        if value is not None and not isinstance(value, basestring):
            cgnum = 'CG%d' % value
            doc['CG_Number'] = cgnum
        value = doc.get('In_Secondary_Screen', 'No')
        value = value.lower().strip()
        if value.startswith('y'):
            value = 1
        elif value.startswith('n'):
            value = 0
        else:
            raise Exception()
        doc['In_Secondary_Screen'] = value
        # Try to get the fbgn for status lookup
        #fbgn = get_fbgn(cgnum, doc.get('FBID_Link'))
        #if fbgn is not None:
        #    temp_doc = db.gene_status.find_one({'primary_FBgn': fbgn})
        #    if temp_doc is not None:
        #        doc['Gene_Model_Status'] = temp_doc['gene_status']
        db.Primary_FB_LW.save(doc)
    except:
        print doc
        raise

# Secondary screen header, parenthesis items are new and added, minus items
# are to be removed
#  Plate
#  Row-
#  Column-
#  (Well)
#  Score_A-
#  Score_B-
#  (Score)
#  Average_Score
#  CG_Number
#  FBID_Link
#  Gene_Name
#  Nearest_Human_Homologue
#  Regulatory_Hub
#  RNAi_Primer_1
#  RNAi_Primer_2
#  (Gene_Model_Status)

# Final
# Plate,
# Well,
# Adhesion_Defect_Secondary,
# Adhesion_Defect_Primary,
# Adhesion_Defect_Secondary_Average,
# CG_Number,
# Nearest_Human_Homologue,
# Regulatory_Hub,RNAi_Primer_1,
# RNAi_Primer_2,
# Gene_Name,
# Gene_Symbol,
# Gene_Model_Status

# Work on the secondary screen
print "\n\n>> Fixing Secondary_FB2\n"
for doc_1 in db.Secondary_FB2.find(snapshot=True):
    print doc_1.get('CG_Number'),
    if doc_1.get('Row') is not None:
        try:
            # Start with those items common between the two
            row = doc_1.get('Row')
            column = doc_1.get('Column')
            if row is not None and column is not None:
                doc_1['Well'] = "%s%02d" % (row, column)

            value = doc_1.get('CG_Number')
            cgnum = None
            if value is not None and not isinstance(value, basestring):
                cgnum = 'CG%d' % value
                doc_1['CG_Number'] = cgnum

            # Try to get the fbgn for status lookup
            fbgn = get_fbgn(cgnum, doc_1.get('FBID_Link'))
            if fbgn is not None:
                temp_doc = db.gene_status.find_one({'primary_FBgn': fbgn})
                if temp_doc is not None:
                    doc_1['Gene_Model_Status'] = temp_doc['gene_status']

            # Now, duplicate and make those items specific
            doc_2 = deepcopy(doc_1)
            doc_2['_id'] = ObjectId()

            plate_val = doc_1.get('Plate')

            doc_1['Plate'] = "Secondary %03da" % plate_val
            doc_2['Plate'] = "Secondary %03db" % plate_val

            score_a = doc_1.get('Score_A')
            score_b = doc_2.get('Score_B')

            if isinstance(score_a, NumberTypes):
                score_a = '%.2f' % score_a

            if isinstance(score_b, NumberTypes):
                score_b = '%.2f' % score_b

            doc_1['Adhesion_Defect_Secondary'] = score_a
            doc_2['Adhesion_Defect_Secondary'] = score_b

            average = doc_1.get('Adhesion_Defect_Secondary_Average')
            if isinstance(average, NumberTypes):
                average = '%.2f' % average
                doc_1['Adhesion_Defect_Secondary_Average'] = average
                doc_2['Adhesion_Defect_Secondary_Average'] = average

            # Get gene symbol from FBGN
            if fbgn is not None:
                temp_doc = db.fbgn_annotation_ID.find_one({'primary_FBgn#': fbgn})
                if temp_doc is not None:
                    symbol = temp_doc.get('gene_symbol')
                    if symbol is not None:
                        doc_1['Gene_Symbol'] = symbol
                        doc_2['Gene_Symbol'] = symbol

            # Finally, clean up those pesky extra columns and save
            for doc in doc_1, doc_2:
                doc.pop('Row')
                doc.pop('Column')
                if doc.has_key('Score_A'):
                    doc.pop('Score_A')
                if doc.has_key('Score_B'):
                    doc.pop('Score_B')
                db.Secondary_FB2.save(doc)
        except:
            print doc_1
            raise
