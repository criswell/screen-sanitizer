#!/bin/bash
#
# MongoDB metadata import script for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

HOSTNAME="localhost"
DATABASE="201306082"

# Steps to produce the following file for import into MongoDB:
#  * Primary_FB_LW.xslx exported to "UTF-16 Unicode Text (.txt)"
#    via Microsoft Excel for Mac 2011
#  * iconv --from-code=UTF-16 --to-code=UTF-8 Primary_FB_LW_utf-16.txt > \
#       Primary_FB_LW_utf-8.txt
#  * dos2unix Primary_FB_LW_utf-8.txt
#  * mac2unix Primary_FB_LW_utf-8.txt
#  * Headerline updated manually
mongoimport -h "$HOSTNAME" -d "$DATABASE" -c 'Primary_FB_LW' \
    --type tsv --file Primary_FB_LW_utf-8.txt --headerline --stopOnError \
    --ignoreBlanks

# Steps to produce the following file for import into MongoDB:
#  * Secondary_FB2.xsls exported to "UTF-16 Unicode Text (.txt)"
#    via Microsoft Excel for Mac 2011
#  * iconv --from-code=UTF-16 --to-code=UTF-8 Secondary_FB2_utf-16.txt > \
#       Secondary_FB2_utf-8.txt
#  * dos2unix Secondary_FB2_utf-8.txt
#  * mac2unix Secondary_FB2_utf-8.txt
#  * Headerline updated manually
mongoimport -h "$HOSTNAME" -d "$DATABASE" -c 'Secondary_FB2' \
    --type tsv --file Secondary_FB2_utf-8.txt --headerline --stopOnError \
    --ignoreBlanks

# Load FlyBase annotation metadata
if [ ! -f "fbgn_annotation_ID_fb_2013_06.tsv" ]; then
    curl -O -k -L 'http://flybase.org/static_pages/downloads/FB2013_06/genes/fbgn_annotation_ID_fb_2013_06.tsv.gz'
    gzip -d fbgn_annotation_ID_fb_2013_06.tsv.gz
    perl -ni -e 'print unless /^[#\s]+/' fbgn_annotation_ID_fb_2013_06.tsv
fi
mongoimport -h "$HOSTNAME" -d "$DATABASE" -c 'fbgn_annotation_ID' \
    -f 'gene_symbol,primary_FBgn#,secondary_FBgn#s,annotation_ID,secondary_annotation_IDs' \
    --type tsv --file fbgn_annotation_ID_fb_2013_06.tsv \
    --stopOnError --ignoreBlanks

# Load gene model status metadata
#  * Status file generated with the following command:
#    psql -h flybase.org -U flybase flybase -c "SELECT uniquename, \
#       featureprop_id, featureprop.type_id, cvterm.name, value FROM  \
#       featureprop INNER JOIN feature ON featureprop.feature_id = \
#       feature.feature_id INNER JOIN cvterm ON featureprop.type_id = \
#        cvterm.cvterm_id WHERE featureprop.type_id = 116949;" > output.csv
mongoimport -h "$HOSTNAME" -d "$DATABASE" -c 'gene_status' \
    -f 'primary_FBgn,featureprop_id,type_id,derived_gene_model_status,gene_status' \
    --type csv --file gene_model_status.csv --stopOnError --ignoreBlanks
