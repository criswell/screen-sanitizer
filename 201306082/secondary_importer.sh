#!/bin/bash

PROGNAME=${0##*/}
MY_CWD=`pwd`

echo "USAGE:"
echo "${PROGNAME} <HOST> <PORT> <USERNAME> <PASSWORD> <SCREEN_ID> <DIRECTORY>  <GLOB> [NUM] [SKIP_NUM]"

if [ "$6" = "" ]; then
    echo "INCORRECT PARAMS"
    exit 1
fi

HOST=$1
PORT=$2
USERNAME=$3
PASSWORD=$4
SCREEN_ID=$5
DIRECTORY=$6
GLOB=$7

NUM=$8
SKIP_NUM=$9
COUNT=0
cd $DIRECTORY

ORG_IFS=$IFS
IFS=$'\n'

DIRS=$(find $GLOB | grep -i htd)

process_file()
{
    echo "Processing ${FILE}"
    ~/OMERO.importer/importer-cli -s $HOST -p $PORT -u $USERNAME -w $PASSWORD \
         -r $SCREEN_ID --no_thumbnails -n "$FN" "${DIRECTORY}/${FILE}"
}

for FILE in $DIRS
do
    FN=$(echo $FILE | cut -d '/' -f 1)
    if [ -n "$SKIP_NUM" ]; then
        if [ $COUNT -lt $SKIP_NUM ]; then
            echo "Skipping ${FILE}"
        else
            process_file
        fi
    else
        process_file
    fi
    COUNT=$((COUNT+1))
    if [ -n "$NUM" ]; then
        if [ $COUNT -gt $NUM ]; then
            break
        fi
        echo "${COUNT} of ${NUM}"
    fi
done

cd $MY_CWD
echo "Done!"

IFS=$ORG_IFS