#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple queries for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

import pymongo

connection = pymongo.Connection()
db = connection['201306082']

# Find Primary screen CG_Number in FlyBase collection where the match is not
# exact.  This locates situations where a single CG_Number results in a
# multitude of FlyBase gene identifier matches.
for cg_number in db.Primary_FB_LW.distinct('CG_Number'):
    if cg_number is None:
        continue
    cursor = db.fbgn_annotation_ID.find({
        '$or': [
            {'annotation_ID': cg_number},
            {'secondary_annotation_IDs': cg_number}
        ]
    })
    if cursor.count() != 1:
        annotation_IDs = [v['annotation_ID'] for v in cursor]
        if cg_number not in annotation_IDs:
            print '%d: %s %r' % (cursor.count(), cg_number, annotation_IDs)
