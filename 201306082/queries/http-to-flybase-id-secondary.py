#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple queries for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

import pymongo

connection = pymongo.Connection()
db = connection['201306082']

secondary = db.Secondary_FB
secondary2 = db.Secondary_FB2

# Take original (Pre-Liz) secondary csv, convert http and insert Flybase IDs
# to Post-Liz csv file

count = 0
for row in secondary2.find():
    Plate_a = "Secondary %03da" % row['PLATE']
    Plate_b = "Secondary %03db" % row['PLATE']
    Well = "%s%02d" % (row['Row'], row['Column'])
    FBID_Link = row['Flybase Link']
    flybase_id = ''
    if FBID_Link != '':
        flybase_id = FBID_Link.split('/')[4].split('.')[0]
    cursor_A = secondary.find({
        '$and': [
            {"Plate": Plate_a},
            {"Well": Well}
        ]
    })
    cursor_B = secondary.find({
        '$and': [
            {"Plate": Plate_b},
            {"Well": Well}
        ]
    })
    if cursor_A.count() != 1:
        print "OO %s, %s, %s" % (Plate_a, Well, flybase_id)
    if cursor_B.count() != 1:
        print "OO %s, %s, %s" % (Plate_b, Well, flybase_id)
    if cursor_A.count() == 1:
        data = secondary.find_one({'_id': cursor_A[0]['_id']})
        data['FBID'] = flybase_id
        secondary.save(data)
        count = count + 1
    if cursor_B.count() == 1:
        data = secondary.find_one({'_id': cursor_B[0]['_id']})
        data['FBID'] = flybase_id
        secondary.save(data)
        count = count + 1
print("Count: %i" % count)
