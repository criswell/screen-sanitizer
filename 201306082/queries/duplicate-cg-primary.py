#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple queries for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

import pymongo

connection = pymongo.Connection()
db = connection['201306082']

# Find duplicate CG_Number in the Primary screen
count = 0
for cg_number in db.Primary_FB_LW.distinct('CG_Number'):
    if cg_number is None:
        continue
    cursor = db.Primary_FB_LW.find({'CG_Number': cg_number})
    number_of_matches = cursor.count()
    if number_of_matches > 1:
        count += 1
        in_secondary_screen = [v['In_Secondary_Screen'] for v in cursor]
        print '%i, %s, %r' % \
            (number_of_matches, cg_number, in_secondary_screen)
print 'Count: %d' % count
