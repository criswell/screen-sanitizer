#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple queries for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

import pymongo

connection = pymongo.Connection()
db = connection['201306082']

primary = db.Primary_FB_LW

# Replace http link with Flybase ID

for row in primary.find():
    FBID_Link = row['FBID_Link']
    if FBID_Link == "":
        continue
    flybase_id = FBID_Link.split('/')[4].split('.')[0]
    if flybase_id == "":
        continue
    row['FBID'] = flybase_id
    primary.save(row)
