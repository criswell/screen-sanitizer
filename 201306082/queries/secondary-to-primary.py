#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple queries for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

import pymongo

connection = pymongo.Connection()
db = connection['201306082']

# Find CG_Number from the Secondary screen in the Primary screen
count = 0
for cg_number in db.Secondary_FB2.distinct('CG_Number'):
    if cg_number is None:
        continue
    cursor = db.Primary_FB_LW.find({'CG_Number': cg_number})
    number_of_matches = cursor.count()
    if number_of_matches != 1:
        count += 1
        print '%d: %s' % (number_of_matches, cg_number)
print 'Count: %d' % count
