#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple queries for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

import pymongo

connection = pymongo.Connection()
db = connection['201306082']

primary = db.Primary_FB_LW
annotation = db.fbgn_annotation_ID

# Add columns to Primary with Annotation ID and Secondary Annotation IDs

for row in primary.find():
    CG_number = row['CG_Number']
    if CG_number == '':
        continue
    cursor = annotation.find({
        '$or': [
            {"annotation_ID": CG_number},
            {"secondary_annotation_ID(s)": CG_number}
        ]
    })
    annotation_ID = ''
    secondary_annotation_IDs = list()

    number_of_matches = cursor.count()
    if number_of_matches == 0:
        continue
    annotation_ID = list()
    secondary_annotation_IDs = list()
    for v in cursor:
        annotation_ID.append(v['annotation_ID'])
        secondary_annotation_IDs.extend(v['secondary_annotation_ID(s)'])
    row['annotation_ID'] = annotation_ID
    row['secondary_annotation_ID(s)'] = secondary_annotation_IDs
    primary.save(row)
