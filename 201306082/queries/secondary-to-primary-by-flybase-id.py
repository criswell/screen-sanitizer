#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple queries for 201306082
#
# Copyright (c) 2013 Glencoe Software, Inc. All rights reserved.
#
# This software is distributed under the terms described by the LICENCE file
# you can find at the root of the distribution bundle.
# If the file is missing please request a copy by contacting
# support@glencoesoftware.com.

import pymongo

connection = pymongo.Connection()
db = connection['201306082']

primary = db.Primary_FB_LW
secondary = db.Secondary_FB

# Find Flybase IDs from secondary screen in primary screen when CG_Number is
# either not found

count_zeros = 0
count_ones = 0
count_multiple = 0
for row in secondary.find():
    FBID = row['FBID']
    CG_Number = row['CG_Number']
    cursor = primary.find({"CG_Number": CG_Number})
    if cursor.count() == 1:
        count_ones = count_ones + 1
        continue
    cursor = primary.find({"annotation_ID": CG_Number})
    if cursor.count() == 1:
        count_ones = count_ones + 1
        continue
    cursor = primary.find({"secondary_annotation_ID(s)": CG_Number})
    if cursor.count() == 1:
        count_ones = count_ones + 1
        continue
    if cursor.count() > 1:
        matched = [v['CG_Number'] for v in cursor]
        count_multiple = count_multiple + 1
        continue
    cursor = primary.find({
        '$or': [
            {"CG_Number": CG_Number},
            {"annotation_ID": CG_Number},
            {"secondary_annotation_ID(s)": CG_Number}
        ]
    })
    if cursor.count() > 1:
        cursor_fbid = primary.find({"FBID": FBID})
        if cursor_fbid.count() == 1:
            count_ones = count_ones + 1
            continue
        matched = [v['CG_Number'] for v in cursor]
        count_multiple = count_multiple + 1
        continue
    if cursor.count() == 0:
        cursor_fbid = primary.find({"FBID": FBID})
        if cursor_fbid.count() == 1:
            count_ones = count_ones + 1
            continue
        matched = [v['CG_Number'] for v in cursor]
        count_zeros = count_zeros + 1
        continue
print("Zeros matches: %s, One match: %s, Multiple: %s"
      % (count_zeros, count_ones, count_multiple))
