#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Vain attempt at ANOTHER screen metadata sanitation tool

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

from __future__ import print_function

import sys
import os.path
import argparse
import csv

from openpyxl import load_workbook
from copy import copy
from utils import get_proper_position

try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

parser = argparse.ArgumentParser(
    description='Metadata sanitizer',
    epilog='fbgn_annotation, see http://flybase.org/static_pages/docs/datafiles.html#annotation_ids. '+
           'fb_synonym, see http://flybase.org/static_pages/docs/datafiles.html#flybase_synonyms.')
parser.add_argument('input_file', type=str,
    help='The file to start with- defaults to XLSX unless CSV specified')
parser.add_argument('output_file', type=str,
    help='The CSV file to output to')
parser.add_argument('fbgn_annotation', type=str,
    help='The FBgn <=> Annotation ID file')
parser.add_argument('fb_synonym', type=str,
    help='The FlyBase Synonyms file to use')
parser.add_argument('status_file', type=str,
    help='The gene model status file (expected to be CSV, first field FBgn, last field status)')
parser.add_argument('--csv', action="store_true",
    help='Determines the input file is CSV')
parser.add_argument('--errfile', type=str, default='sanitizer_error.log',
    help='Error log (because there will be errors). Defaults to sanitizer_error.log')

args = parser.parse_args()

source_xlsx = os.path.abspath(args.input_file)
output_file = os.path.abspath(args.output_file)
annotation_file = os.path.abspath(args.fbgn_annotation)
synonym_file = os.path.abspath(args.fb_synonym)
error_log = os.path.abspath(args.errfile)
gene_status_file = os.path.abspath(args.status_file)

annotation_lookup = dict()
synonym_lookup = dict()
status_lookup = dict()

# Once again, pre-load insanity..... SOMEONE STOP HIM, HE'S A MADMAN!
with open(annotation_file, 'r') as tsv_file:
    print('------------------------------------')
    print('CACHING ANNOTATION FILE')
    print(' > (ANNOTATION_ID) : (FBgn)')
    for rawline in tsv_file.readlines():
        line = rawline.rstrip('\n')
        if not line.startswith('#') and len(line)>0:
            cols = line.split('\t')
            annotation_lookup[cols[3]] = cols[1]
            print(' > (%s) : (%s)' % (cols[3], cols[1]))
            # Deal with any secondary annotations
            try:
                for secondary in cols[4].split(','):
                    if not annotation_lookup.has_key(secondary):
                        annotation_lookup[secondary] = cols[1]
                        print(" SECONDARY> (%s) : (%s)" % (secondary, cols[1]))
            except:
                print(" > NO SECONDARY")

with open(synonym_file, 'r') as tsv_file:
    print('------------------------------------')
    print('CACHING SYNONYM FILE')
    print(' > (FBgn) : (SYMBOL, FULLNAME)')
    for rawline in tsv_file.readlines():
        line = rawline.rstrip('\n')
        if not line.startswith('#') and len(line)>0:
            cols = line.split('\t')
            # (symbol, name)
            synonym_lookup[cols[0]] = (cols[1], cols[2])
            print(' > (%s) : (%s, %s)' % (cols[0], cols[1], cols[2]))

with open(gene_status_file, 'r') as csvfile:
    print('------------------------------------')
    print('CACHING GENE STATUS FILE')
    print(' > (FBgn) : (STATUS)')
    reader = csv.reader(csvfile)
    for row in reader:
        if len(row) > 0:
            status_lookup[row[0]] = row[-1]
            print(" > (%s) : (%s)" % (row[0], row[-1]))

errors = []

def lookup_status(url):
    try:
        parsed = urlparse(url)
        p = parsed.path.split('/')[-1].split('.')[0]
        if status_lookup.has_key(p):
            return status_lookup[p]
        else:
            return ''
    except:
        return ''

cells_to_trim = []

with open(output_file, 'wb') as csvfile:
    writer = csv.writer(csvfile)

    def deal_with_line(line):
        writer.writerow(line)
        print(line)

    def compute_cells_to_trim(line):
        for i in range(len(line)):
            if len(line[i]) == 0 or line[i] is None:
                cells_to_trim.append(i)

    def trim_cells(line):
        new_line = []
        for i in range(len(line)):
            if not i in cells_to_trim:
                new_line.append(line[i])
        return new_line

    if args.csv:
        with open(source_xlsx, 'rb') as csvin:
            print('------------------------------------')
            print('PROCESSING CSV FILE')
            reader = csv.reader(csvin)
            isHeader = True
            row_id = 0
            for row in reader:
                line = copy(row)
                if isHeader:
                    isHeader = False
                    line.pop(1)
                    line[1] = 'Well'
                    line.append('Gene Model Status')
                    compute_cells_to_trim(line)
                    line = trim_cells(line)
                else:
                    aid = "CG%s" % row[5]
                    if annotation_lookup.has_key(aid):
                        line[7] = synonym_lookup[annotation_lookup[aid]][1]
                        line[8] = synonym_lookup[annotation_lookup[aid]][0]
                    else:
                        errors.append('key "%s" not found in annotation_lookup, row %s' % (aid, row_id))
                    line.append(lookup_status(line[6]))
                    line[2] = "%s%02d" % (line[1], int(line[2]))
                    line.pop(1)
                    line = trim_cells(line)

                deal_with_line(line)
                row_id = row_id + 1

    else:
        wb = load_workbook(filename = source_xlsx, use_iterators = True)
        print('------------------------------------')
        print('PROCESSING XLSX FILE')
        writer = csv.writer(csvfile)

        for ws in wb.worksheets:
            isHeader = True
            for row in ws.iter_rows():
                line = []
                row_id = None
                if isHeader:
                    # Figure out our columns to update
                    isHeader = False
                    for cell in row:
                        value = cell.internal_value
                        if value is not None:
                            value = value.strip()
                            line.append(str(value.replace('"', '')))
                    line.pop(1)
                    line[1] = 'Well'
                    line.append('Gene Model Status')
                    compute_cells_to_trim(line)
                    line = trim_cells(line)
                else:
                    for cell in row:
                        value = cell.internal_value
                        if type(value) is type(str) or type(value) is type(u''):
                            value = value.strip()

                        pos = get_proper_position(cell.coordinate)
                        row_id = pos[0]
                        if pos[0] in (gene_symbol_cell, gene_name_cell):
                            line.append('')
                        else:
                            line.append(value)

                    aid = "CG%s" % line[5]
                    if annotation_lookup.has_key(aid):
                        line[7] = synonym_lookup[annotation_lookup[aid]][1]
                        line[8] = synonym_lookup[annotation_lookup[aid]][0]
                    else:
                        errors.append('key "%s" not found in annotation_lookup, row %s' % (aid, row_id))
                    line.append(lookup_status(line[6]))
                    line[2] = "%s%02d" % (line[1], int(line[2]))
                    line.pop(1)
                    line = trim_cells(line)
                deal_with_line(line)

# Get them errors out
with open(error_log, 'wb') as errlog:
    print('------------------------------------')
    print('ERRORS ENCOUNTERED\n')
    for line in errors:
        errlog.write('%s\n' % line)
        print(line)
