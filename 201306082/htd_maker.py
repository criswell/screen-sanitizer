#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Given the path to the files, and the template htd, will attempt to
   generate htds for each file.

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

from __future__ import print_function

import sys
import os.path
import os
from time import sleep
from copy import deepcopy
import errno

from htdtool import read_htd, write_htd

if len(sys.argv) < 3:
    print("Error, usage!")
    print("htd_maker.py template.htd root/of/data/ [optional/dir/to/new/htds]")
    sys.exit(1)

htd_file = os.path.abspath(sys.argv[1])
data_root = os.path.abspath(sys.argv[2])
htd_dest = data_root

if len(sys.argv) > 3:
    htd_dest = os.path.abspath(sys.argv[3])

(template_data, template_keys) = read_htd(htd_file)

def del_root(base, path):
    """
    Given a base and a path, will subtract the base from the path and return
    the (now relative) path.
    """
    u = base.split('/')
    l = path.split('/')

    return "/".join(l[len(u)-1:])

def mkdir_p(path):
    '''
    Does the equivalent of a 'mkdir -p' (Linux) on both platforms.
    '''
    try:
        os.makedirs(path)
    except OSError, exc:
        if exc.errno == errno.EEXIST:
            pass
        else: raise

for root, dirs, files in os.walk(data_root):
    # make sure dest has the same structure
    for name in dirs:
        local_root = del_root(data_root, os.path.join(root,name))
        dest_dir = os.path.abspath('%s/%s' % (htd_dest, local_root))
        mkdir_p(dest_dir)
    for f in files:
        ext = f.split('.')
        if ext[-1].lower() == 'tif':
            a = f.split('_')
            local_root = del_root(data_root, root)
            if "201306082/Plates" in local_root:
                print("Skipping Plates directory")
                break
            plate_id =  local_root.split('/')[-1]
            temp = deepcopy(template_data)
            temp['"Description"'] = '"%s"' % plate_id
            temp['"WaveName1"'] = '"Hoechst"'
            dest_dir = os.path.abspath('%s/%s' % (htd_dest, local_root))
            new_name = "%s/%s.HTD" % (dest_dir, '-'.join(a[0:1]))
            print('Writing HTD: %s' % new_name)
            if os.path.isfile(new_name):
                print("Skipping... already done...")
            else:
                write_htd(new_name, temp, template_keys)
            break