#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Base class for generic screen sanitization

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

from __future__ import print_function

import sys
import os.path
import argparse
import exceptions
import csv

from dfreader import get_reader

class Sanity(object):
    """
    Generic screen sanitization base class.

    The idea here is that you should extend this object and overwrite
    the stubbed transformation methods and initialization classes.
    """

    def _initialize_pre(self):
        """
        Initialization method to be called before the parser is set up.
        The items detailed here are those that should be overwritten.
        """
        self._epilog = ''
        self._description = 'Metadata sanitizer'
        self._name = 'sanitizer'

    def _initialize_parser(self):
        """
        Add any extensions to the parser as needed here.
        """
        pass

    def _initialize_final(self):
        """
        This is the final method available for any initialization.
        It will be called after everything else has been done in
        __init__()

        This would be a good place for any sorts of pre-caching that might
        need to be done.
        """
        pass

    def __init__(self):
        self._initialize_pre()

        self.parser = argparse.ArgumentParser(
            description = self._description, epilog = self._epilog)
        self.parser.add_argument('input_file', type=str,
            help='The file to start with- defaults to XLSX unless CSV specified')
        self.parser.add_argument('output_file', type=str,
            help='The CSV file to output to')
        self.parser.add_argument('--csv', action="store_true",
            help='Determines the input file is CSV')
        self.parser.add_argument('--noheader', action="store_true",
            help='If the source file lacks a proper header, use this flag.')
        self.parser.add_argument('--errfile', type=str, default='sanitizer_error.log',
            help='Error log (because there will be errors). Defaults to sanitizer_error.log')

        self._initialize_parser()
        self.args = self.parser.parse_args()
        self.source_file = os.path.abspath(self.args.input_file)
        self.output_file = os.path.abspath(self.args.output_file)
        self.error_log = os.path.abspath(self.args.errfile)

        self.header = None
        self.writer = None
        self.errors = []

        self._initialize_final()

    def _transform_header(self):
        """
        This method is called when the header is encountered
        """
        raise exceptions.NotImplementedError()

    def _transform_row(self, row):
        """
        Should contain your login for row transformation.
        """
        raise exceptions.NotImplementedError()

    def deal_with_line(self, line):
        print(line)
        def f(x):
            if x is None:
                return u''
            try:
                # First, we try default (ascii)
                return unicode(x).encode('utf_8')
            except UnicodeDecodeError:
                # Okay,try utf-8 as source
                return unicode(x, encoding='utf_8').encode('utf_8')
        self.writer.writerow([f(x) for x in line])

    def run(self):
        """
        Main entry point for the sanitizer class. Your sub-classed script
        should call this when it's ready to run.
        """
        print("STARTING MAIN RUN")
        print("-----------------------------------")
        with open(self.output_file, 'wb') as csvfile:
            self.writer = csv.writer(csvfile)

            source_reader = get_reader('xls')
            if self.args.csv:
                source_reader = get_reader('csv')

            isHeader = True
            if self.args.noheader:
                isHeader = False

            for row in source_reader(self.source_file):
                if isHeader:
                    self.header = row
                    self._transform_header()
                    isHeader = False
                else:
                    self._transform_row(row)

        with open(self.error_log, 'wb') as errlog:
            print('------------------------------------')
            print('ERRORS ENCOUNTERED\n')
            for line in self.errors:
                errlog.write('%s\n' % line)
                print(line)
