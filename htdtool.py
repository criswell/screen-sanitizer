#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Simple, no dep HTD reader/writer

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

def read_htd(htd_file):
    """
    Reads HTD data from a file. Returns a tuple containing a dictionary based
    upon the HTD data and the order of the items found.
    """
    htd_data = {}
    keys_odered = []
    with open(htd_file, 'rb') as htdin:
        for line in htdin:
            a = line.strip().split(',')
            key = a[0].strip()
            keys_odered.append(key)
            if len(a) == 1:
                htd_data[key] = None
            elif len(a) < 3:
                htd_data[key] = a[1].strip()
            else:
                htd_data[key] = [w.strip() for w in a[1:]]

    return (htd_data, keys_odered)

def write_htd(htd_file, htd_data, keys_odered):
    """
    Writes data from htd_data back to a file, attempting to get the correct
    format as close as possible.
    """
    with open(htd_file, 'w') as htdout:
        for key in keys_odered:
            line = ""
            data = htd_data[key]
            if type(data) is list:
                line = "%s, %s" % (key, ', '.join(data))
            elif data is None:
                line = key
            else:
                line = "%s, %s" % (key, data)
            htdout.write("%s\r\n" % line)
