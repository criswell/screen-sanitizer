#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Filter out the missing metadata entries
   (HACKERY AND FOO)

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys, csv, ast

if len(sys.argv) < 4:
    print "Error, usage!"
    print "filter_missing.py input_file.csv missing_entries.dat output_file.csv"
    sys.exit(1)

input_file = sys.argv[1]
missing_file = sys.argv[2]
output_file = sys.argv[3]

missing = dict()

with open(missing_file, 'r') as missin:
    for rawline in missin.readlines():
        line = rawline.strip()
        if line.startswith('#'):
            pass
        else:
            plateraw, wellraw = line.split('|')
            plate = plateraw.strip()
            well = ast.literal_eval(wellraw.strip())
            if not missing.has_key(plate):
                missing[plate] = []

            n = (well[0].strip(), well[1].zfill(2))
            wellactual = "".join(n).upper()

            if not wellactual in missing[plate]:
                missing[plate].append(wellactual)

#print missing

with open(input_file, 'rb') as csvin:
    with open(output_file, 'wb') as csvout:
        csvread = csv.reader(csvin)
        csvwrite = csv.writer(csvout)
        for row in csvread:
            plate = row[0]
            well = row[1].upper()
            skip = False
            if missing.has_key(plate):
                if well in missing[plate]:
                    print "SKIPPING: %s" % row
                    skip = True
            
            if not skip:
                csvwrite.writerow(row)