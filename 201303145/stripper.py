#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Tool to strip empty plate/well id rows from a CSV.

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys, csv

if len(sys.argv) < 3:
    print "Error, usage!"
    print "stripper.py input_file.csv output_file.csv"
    sys.exit(1)

input_file = sys.argv[1]
output_file = sys.argv[2]

with open(input_file, 'rb') as csvin:
    with open(output_file, 'wb') as csvout:
        csvread = csv.reader(csvin)
        csvwrite = csv.writer(csvout)
        for row in csvread:
            if row[0] is None or row[0] == '':
                pass
            else:
                csvwrite.writerow(row)
                print row
