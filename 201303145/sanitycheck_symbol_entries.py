#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Companion file for count_symbol_date_entries.py, takes its output and
   the original xlsx and shows that the rows have been updated (or not).

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys

from openpyxl import load_workbook

if len(sys.argv) < 3:
    print "Error, usage!"
    print "sanitycheck_symbol_entries.py input_file.xlsx count_output.txt"
    sys.exit(1)

input_file = sys.argv[1]
check_file = sys.argv[2]

def get_proper_position(string):
    """
    Given the annoying string representation of a cell position in a
    spreadsheet, return a tuple containing it in a proper, parsable, form
    eg. ('AA', '12')
    """
    string = string.strip()
    alpha = ''
    number = ''
    for c in string:
        if c.isalpha():
            alpha = alpha + c
        else:
            number = number + c
    return (alpha, number)

sym_col = 'D'

lines_to_check = []

with open(check_file, 'rb') as chfl:
    for rawline in chfl.readlines():
        line = rawline.strip()
        if line.startswith("#"):
            pass
        else:
            data = line.split(":")
            row = int(data[0].strip())
            lines_to_check.append(row)

wb = load_workbook(filename = input_file, use_iterators = True)
for ws in wb.worksheets:
    rownum = 1
    for row in ws.iter_rows():
        line = []
        display_line = False
        for cell in row:
            value = cell.internal_value
            line.append(value)
            pos = get_proper_position(cell.coordinate)
            if pos[0] == sym_col:
                if rownum in lines_to_check:
                    display_line = True

        if display_line:
            print "%s : %s" % (rownum, line)

        rownum = rownum + 1