#!/bin/bash

# bfconvert wrapper for concat files

PROGNAME=${0##*/}
MY_CWD=`pwd`

echo "USAGE:"
echo "${PROGNAME} <DIRECTORY> <BFCONVERT_PROGRAM>"

if [ "$2" = "" ]; then
    echo "INCORRECT PARAMS"
    exit 1
fi

DIRECTORY=$1
BFCONVERT=$2

cd $DIRECTORY

DIRS=$(ls)

for DIR in $DIRS
do
    echo "Processing ${DIR}..."
    cd $DIR
    FILES=$(ls)
    for F in $FILES
    do
        if [ "$F" == "DAPI.tif" ]; then
            mv $F c1.tif
        elif [ "$F" == "FLAG.tif" ]; then
            mv $F c2.tif
        else
            mv $F c3.tif
        fi
    done
    $BFCONVERT -stitch c1.tif combined.ome.tiff
    rm -f c?.tif
    cd ../
done

cd $MY_CWD
echo "Done!"

