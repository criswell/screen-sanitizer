#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   bfconvert wrapper for concat files (second version)

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys
import subprocess
import os.path
from os import rename

if len(sys.argv) < 3:
    print "Error, usage!"
    print "bfconver_wrap.py top_level_dir/ bfcommand"
    sys.exit(1)

top_dir = os.path.abspath(sys.argv[1])
bfcommand = os.path.abspath(sys.argv[2])

rename_lookup = {
    'DAPI.tif' : 'c1',
    'FLAG.tif' : 'c2',
}

def jangling_jack(arg, dirname, names):
    # Get our suffix first
    ordered_names = []
    l = list(names)
    if "DAPI.tif" in names:
        ordered_names.append("DAPI")
        l.remove('DAPI.tif')
    if "FLAG.tif" in names:
        ordered_names.append("FLAG")
        l.remove('FLAG.tif')
    if len(l) > 0:
        ordered_names.append(l[-1].split('.')[0])

    suffix = "_".join(ordered_names)

    print '-' * 40
    print "In directory '%s'" % dirname
    print "-> Suffix is '%s'" % suffix

    # Now deal with the renames
    starting_number = 3
    for name in names:
        # pass on directories
        source_file = os.path.normpath("%s/%s" % (dirname, name))
        if os.path.isdir(source_file):
            # So, if we encounter a directory, any directory, the above
            # suffix process goes to hell. Since we're assuming this will
            # only happen at the top level, we return and don't throw an
            # error.
            return

        prefix = 'c3'
        if rename_lookup.has_key(name):
            prefix = rename_lookup[name]
            if int(rename_lookup[name][1]) < starting_number:
                starting_number = int(rename_lookup[name][1])

        fullname = '%s_%s.tif' % (prefix, suffix)
        print "\tRenaming %s -> %s" % (name, fullname)
        dest_file  = os.path.normpath("%s/%s" % (dirname, fullname))
        rename(source_file, dest_file)

    # Provides we're still here, run bfconvert
    starting_file = 'c%d_%s.tif' % (starting_number, suffix)
    combined_name = "combined_%s.ome.tif" % suffix
    exec_command = [bfcommand, '-stitch', starting_file, combined_name]
    print "-> Running %s" % exec_command
    subprocess.call(exec_command, cwd=dirname)

os.path.walk(top_dir, jangling_jack, None)
