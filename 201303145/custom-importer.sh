#!/bin/bash

# Import wrapper that adds the directory name as the description

PROGNAME=${0##*/}
MY_CWD=`pwd`

echo "USAGE:"
echo "${PROGNAME} <HOST> <PORT> <USERNAME> <PASSWORD> <DATASET_ID> <DIRECTORY>"

if [ "$6" = "" ]; then
    echo "INCORRECT PARAMS"
    exit 1
fi

HOST=$1
PORT=$2
USERNAME=$3
PASSWORD=$4
DATASET_ID=$5
DIRECTORY=$6

cd $DIRECTORY

DIRS=$(ls)

for DIR in $DIRS
do
    FN=$(ls $DIRECTORY/$DIR | cut -d . -f 1)
    echo "Processing ${DIR}... with ${FN}"
    ~/OMERO.importer/importer-cli -s $HOST -p $PORT -u $USERNAME -w $PASSWORD -d $DATASET_ID --no_thumbnails -n $FN -x $DIR $DIRECTORY/$DIR/
done

cd $MY_CWD
echo "Done!"
