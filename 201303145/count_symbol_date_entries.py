#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Count those lines which contain a date field in symbol column

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys
import datetime

from openpyxl import load_workbook

if len(sys.argv) < 2:
    print "Error, usage!"
    print "count_symbol_date_entries.py input_file.xlsx"
    sys.exit(1)

input_file = sys.argv[1]

def get_proper_position(string):
    """
    Given the annoying string representation of a cell position in a
    spreadsheet, return a tuple containing it in a proper, parsable, form
    eg. ('AA', '12')
    """
    string = string.strip()
    alpha = ''
    number = ''
    for c in string:
        if c.isalpha():
            alpha = alpha + c
        else:
            number = number + c
    return (alpha, number)

sym_col = 'D'

wb = load_workbook(filename = input_file, use_iterators = True)
for ws in wb.worksheets:
    rownum = 1
    for row in ws.iter_rows():
        line = []
        display_line = False
        for cell in row:
            value = cell.internal_value
            line.append(value)
            pos = get_proper_position(cell.coordinate)
            if pos[0] == sym_col:
                if type(value) is type(datetime.datetime.now()):
                    display_line = True

        if display_line:
            print "%s : %s" % (rownum, line)

        rownum = rownum + 1
