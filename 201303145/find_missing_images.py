#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Find missing images from populate_metadata output.
   (THE DEGREES OF HACKERY ARE OVER 9000!!!!)

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys, re

stat_delim = "DEBUG:omero.util.populate_metadata:Parsed"

regex = re.compile("DEBUG:omero.util.populate_metadata:Row: (.*) Column: (.*) not found!")

if len(sys.argv) < 2:
    print "Error, usage!"
    print "find_missing_images.py populate_metadata_out.txt"
    sys.exit(1)

input_file = sys.argv[1]

with open(input_file, 'r') as populate_metadata:
    current_plate = None
    current_well = None
    print "#Plate\t|\tWell"
    for rawline in populate_metadata.readlines():
        if rawline.startswith(stat_delim):
            line = rawline.split(' ')
            current_well = (line[3], line[5])
            current_plate = line[-1].strip()
        else:
            result = regex.findall(rawline)
            if result:
                print "%s\t|\t%s" % (current_plate, current_well)
