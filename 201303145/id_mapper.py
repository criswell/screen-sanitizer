#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   ID mapping tool.

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

from omero.gateway import BlitzGateway
from omero.rtypes import rlong, rstring, unwrap
import omero
import sys
import csv
import re
import string

if len(sys.argv) < 7:
    print "Error, usage!"
    print "id_mapper.py input_file.csv output_file.csv username password host port"
    sys.exit(1)

input_file = sys.argv[1]
output_file = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]
host = sys.argv[5]
port = sys.argv[6]

conn = BlitzGateway(username, password, host=host, port=port)
conn.connect()

session = conn.getSession()

q = conn.getQueryService()

row_lookup = list(string.ascii_lowercase)

regex = re.compile("plate (.*)_Plate_*")

class MapObject(object):
    def __init__(self, plate_id=None, well_id = None, raw_name=None, raw_row=None, raw_col=None):
        self.plate_id = int(plate_id)
        self.well_id = int(well_id)
        self.raw_name = raw_name
        self.raw_row = raw_row
        self.raw_col = raw_col

        self.row = None
        self.col = None
        self.plate = None
        self.position = None

        self.process()

    def process(self):
        if self.plate_id is None or self.raw_name is None or \
          self.raw_col is None or self.raw_row is None:
            raise Exception('Missing data')

        result = regex.findall(str(self.raw_name))
        if result:
            self.plate = str(result[0])

        self.row = row_lookup[int(self.raw_row)]
        self.col = int(self.raw_col) + 1
        self.position = "%s%02d" % (self.row.lower(), self.col)


sql = "select p.id, p.name, w.id, w.row, w.column from Plate p join p.wells w"

params = omero.sys.ParametersI()

mapping = dict()

for element in q.projection(sql, params, dict()):
    mo = MapObject(\
        plate_id = unwrap(element[0]),
        well_id = unwrap(element[2]),
        raw_name = unwrap(element[1]),
        raw_col = unwrap(element[4]),
        raw_row = unwrap(element[3])
        )

    if not mapping.has_key(mo.plate):
        mapping[mo.plate] = dict()

    mapping[mo.plate][mo.position] = mo

print "===================================="
print "Dumping the mapping data"
print "===================================="

for i in mapping.keys():
    if i is not None:
        print " %s ===>" % i
        for e in mapping[i].keys():
            print "plate_id:%s, well_id:%s <--> pos:%s" % \
            (mapping[i][e].plate_id, mapping[i][e].well_id, mapping[i][e].position)

print "===================================="
print "Processing the CSV"
print "===================================="

with open(input_file, 'rb') as csvin:
    with open(output_file, 'wb') as csvout:
        csvread = csv.reader(csvin)
        csvwrite = csv.writer(csvout)
        for row in csvread:
            if row[0] is None or row[0] == '':
                if mapping.has_key(row[2]):
                    row[0] = mapping[row[2]][row[3].lower()].plate_id
                    row[1] = mapping[row[2]][row[3].lower()].well_id
                    #print row
            csvwrite.writerow(row)
            print row