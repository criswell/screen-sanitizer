#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   bfconvert cleanup script

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys
import os.path
from os import unlink

if len(sys.argv) < 2:
    print "Error, usage!"
    print "bfconver_cleanup.py top_level_dir/"
    sys.exit(1)

top_dir = os.path.abspath(sys.argv[1])

def jangling_jack(arg, dirname, names):
    print "names %s" % names
    for name in names:
        # pass on directories
        source_file = os.path.normpath("%s/%s" % (dirname, name))
        print "Processing %s" % source_file
        if not os.path.isdir(source_file) and not name.startswith("combined"):
            print "Unlinking %s" % source_file
            unlink(source_file)

os.path.walk(top_dir, jangling_jack, None)
