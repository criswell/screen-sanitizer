#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Filename fixer

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys
import shelve
import subprocess
import os.path
from os import link, mkdir, makedirs

if len(sys.argv) < 3:
    print "Error, usage!"
    print "fix_filenames.py source_dir/ dest_dir/ [optional_lookup_file]"
    sys.exit(1)

source_dir = os.path.abspath(sys.argv[1])
dest_dir = os.path.abspath(sys.argv[2])

lookup_file = "fix_filenames.dat"

if len(sys.argv) > 3:
    lookup_file = sys.argv[3]

lookup_table = shelve.open(lookup_file)

def mkdir_p(path):
    '''
    Does the equivalent of a 'mkdir -p' (Linux) on both platforms.
    '''
    try:
        makedirs(path)
    except OSError, exc:
        if exc.errno == errno.EEXIST:
            pass
        else: raise

def bangarang(arg, dirname, names):
    head, tail = os.path.split(dirname)
    if len(tail.split('@')) > 1:
        dest_path = os.path.normpath("%s/%s/%s" % (dest_dir, head, tail.upper()))
    else:
        dest_path = os.path.normpath("%s/%s/%s" % (dest_dir, head, tail))
    print "%s -> %s" % (dirname, dest_path)
    mkdir_p(dest_path)
    for name in names:
        source_file = os.path.normpath("%s/%s" % (dirname, name))
        if os.path.isfile(source_file):
            if not lookup_table.has_key(name):
                replacer = raw_input("\t%s not found, please enter replacement: " % name)
                lookup_table[name] = replacer.strip()

            print "\t%s -USING-> %s" % (name, lookup_table[name])
            dest_file = os.path.normpath("%s/%s" % (dest_path, lookup_table[name]))

            subprocess.Popen(['cp', '-al', source_file, dest_file])

os.path.walk(source_dir, bangarang, None)

lookup_table.close()
