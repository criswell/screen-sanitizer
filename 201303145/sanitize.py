#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Vain attempt at a screen sanitation tool.

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys
import csv

from openpyxl import load_workbook

from utils import get_proper_position

if len(sys.argv) < 4:
    print "Error, usage!"
    print "sanitizer.py input_file.xlsx output_file.csv fasta_file.txt"
    sys.exit(1)

header_lookup = {
    'resource_plate' : "Plate",
    'resource_position' : 'Well',
    'internal_id' : 'Internal ID',
}

input_file = sys.argv[1]
output_file = sys.argv[2]
fasta_file = sys.argv[3]

fasta_lookup = {}

bool_cols =  ('F', 'G')
int_cols = ('A', 'C')
#lower_cols = ('B')

# Pre-load fasta? Good idea? Bad idea? Whatever! PRELOAD ALL THE THINGS!
with open(fasta_file, 'r') as fasta:
    for rawline in fasta.readlines():
        line = rawline.strip()
        if line.startswith(">"):
            cols = line.split('|')
            orf = cols[1].split(":")[1].strip()
            geid = cols[3].split(":")[1].strip()
            fasta_lookup[orf] = geid
            print "ORF ID %s = GENE ID %s" % (orf, geid)


wb = load_workbook(filename = input_file, use_iterators = True)
with open(output_file, 'wb') as csvfile:
    writer = csv.writer(csvfile)

    for ws in wb.worksheets:
        cell_with_orf = None
        cols_to_ignore = []
        # THIS BREAKS IF COLUMNS ARE DIFFERENT BETWEEN SHEETS!
        # BE WARNED ON MULTISHEET DOCS
        isHeader = True
        for row in ws.iter_rows():
            line = []
            if isHeader:
                #line.extend(['Plate', 'Well'])
                isHeader = False
                for cell in row:
                    value = cell.internal_value
                    if value is not None:
                        value = value.strip()
                        if value.lower() == 'internal_id':
                            cell_with_orf = get_proper_position(cell.coordinate)
                        if header_lookup.has_key(value.lower()):
                            line.append(header_lookup[value.lower()])
                        else:
                            line.append(str(value.replace('"', '')))
                    else:
                        cols_to_ignore.append(get_proper_position(cell.coordinate)[0])
                line.append('Entrez Gene ID')
            else:
                #line.extend(["",""])
                orf_id = None
                for cell in row:
                    pos = get_proper_position(cell.coordinate)
                    if pos[0] in cols_to_ignore:
                        break

                    value = cell.internal_value
                    if type(value) is type(str) or type(value) is type(u''):
                        value = value.strip()

                    if pos[0] in bool_cols:
                        # Shoot me now
                        if value is None:
                            value = 0
                        elif u'\u221a' in value:
                            value = 1
                        else:
                            value = 0

                    if pos[0] in int_cols and type(value) is not None:
                        value = str(round(value)).rstrip('0').rstrip('.')

                    #if pos[0] in lower_cols and type(value) is not None:
                        #value = value.lower()

                    if cell_with_orf is not None and \
                       cell_with_orf[0] == pos[0]:
                        orf_id = value
                    line.append(value)

                if orf_id is not None and fasta_lookup.has_key(orf_id):
                    line.append(fasta_lookup[orf_id])
                else:
                    line.append('')
            writer.writerow(line)
            print line



    #import pdb; pdb.set_trace()