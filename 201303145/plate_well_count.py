#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Count the expected images per plate based upon metadata

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys
from openpyxl import load_workbook

if len(sys.argv) < 2:
    print "Error, usage!"
    print "plate_well_count.py input_file.xlsx"
    sys.exit(1)

input_file = sys.argv[1]

plates = dict()
total_images = 0

plate_field = 'A'
#well_field = 'B'

def get_proper_position(string):
    """
    Given the annoying string representation of a cell position in a
    spreadsheet, return a tuple containing it in a proper, parsable, form
    eg. ('AA', '12')
    """
    string = string.strip()
    alpha = ''
    number = ''
    for c in string:
        if c.isalpha():
            alpha = alpha + c
        else:
            number = number + c
    return (alpha, number)

wb = load_workbook(filename = input_file, use_iterators = True)

for ws in wb.worksheets:
    isHeader = True
    for row in ws.iter_rows():
        if isHeader:
            isHeader = False
        else:
            for cell in row:
                pos = get_proper_position(cell.coordinate)

                if pos[0] == plate_field:
                    # Deal with the plate
                    p = str(round(cell.internal_value)).rstrip('0').rstrip('.')
                    if plates.has_key(p):
                        plates[p] = plates[p] + 1
                    else:
                        plates[p] = 1

print "Plate\t|\tCount"
for key in plates.keys():
    print "%s\t|\t%s" % (key, plates[key])
