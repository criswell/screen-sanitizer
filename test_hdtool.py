#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Test the HTD tool

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

from htdtool import read_htd, write_htd

import sys
import os.path
import os

if len(sys.argv) < 2:
    print "Error, usage!"
    print "test_hdtools.py template.htd"
    sys.exit(1)

htd_file = os.path.abspath(sys.argv[1])

test_file = "%s-test" % htd_file

(data, keys) = read_htd(htd_file)
write_htd(test_file, data, keys)

print "diff -u %s %s" % (htd_file, test_file)
