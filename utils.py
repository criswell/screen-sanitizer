#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Utility methods

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

def get_proper_position(string):
    """
    Given the annoying string representation of a cell position in a
    spreadsheet, return a tuple containing it in a proper, parsable, form
    eg. ('AA', '12')
    """
    string = string.strip()
    alpha = ''
    number = ''
    for c in string:
        if c.isalpha():
            alpha = alpha + c
        else:
            number = number + c
    return (alpha, number)