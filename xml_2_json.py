#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Tool for converting large XML files to (equally large) JSON files

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys
import os.path
import json
import argparse

from lxml import etree

parser = argparse.ArgumentParser(
    description='Tool for converting large XML files to JSON files')
parser.add_argument('xmlfile', type=str,
    help='The XML file to parse')
parser.add_argument('jsonfile', type=str,
    help='The JSON file to ouput to')
parser.add_argument('--nopretty', action="store_false",
    help='Disable prettying the resulting JSON')
parser.add_argument('--multi', action="store_true",
    help='Output multiple JSON files. With this option, jsonfile becomes a directory.')
parser.add_argument('--tag', type=str, default='feature',
    help='Optional tag to start processing at')

args = parser.parse_args()

XML_FILE = os.path.abspath(args.xmlfile)
JSON_FILE = os.path.abspath(args.jsonfile)
TAG = args.tag
PRETTY = args.nopretty
MULTI = args.multi
CUR = 0
FORMAT = "%050d.json"
#with open(JSON_FILE, 'w') as json_out:

if not MULTI:
    json_out = open(JSON_FILE, 'w')

def write_json(jelem):
    global CUR
    if MULTI:
        fname = "%s/%s" % (JSON_FILE, FORMAT % CUR)
        json_out = open(fname, 'w')
        json_out.write(jelem)
        json_out.close()
        CUR = CUR + 1
        print "processing %i..." % CUR
    else:
        json_out.write(jelem)

def fast_iter(context, func):
    # http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    # Author: Liza Daly
    for event, elem in context:
        func(elem)
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    del context

def elem_to_json(elem):
    if PRETTY:
        jelem = json.dumps(process_element(elem), indent=2,
            separators=(',', ': '))
    else:
        jelem = json.dumps(process_element(elem))
    write_json("%s\n" % jelem)

def process_element(elem):
    pelem = dict()
    if len(elem) > 0:
        pelem[elem.tag] = {}
        for e in elem:
            # So, this isn't perfect... it assumes you always have unique
            # elements...
            pelem[elem.tag].update(process_element(e))
    else:
        pelem[elem.tag] = elem.text
    return pelem

context = etree.iterparse(XML_FILE, tag=TAG)
fast_iter(context,elem_to_json)

if not MULTI:
    json_out.close()
