#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Tool for importing large XML files into MongoDB

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import sys
import os.path
import argparse
from pymongo import MongoClient

from lxml import etree

parser = argparse.ArgumentParser(
    description='Tool for importing large XML files into MongoDB')
parser.add_argument('xmlfile', type=str,
    help='The XML file to parse')
parser.add_argument('--db', type=str, required=True,
    help='The DB to use')
parser.add_argument('--collection', type=str, required=True,
    help='The collection to use')
parser.add_argument('--host', type=str, default="127.0.0.1",
    help='Host to connect to')
parser.add_argument('--port', type=int, default=27017,
    help='Port to connect to')
parser.add_argument('--tag', type=str, default='feature',
    help='Optional tag to start processing at')

args = parser.parse_args()

XML_FILE = os.path.abspath(args.xmlfile)
TAG = args.tag

client = MongoClient(args.host, args.port)
db = client[args.db]
datastore = db[args.collection]

def fast_iter(context, func):
    # http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    # Author: Liza Daly
    for event, elem in context:
        func(elem)
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    del context

def sloth_loves_chunk(elem):
    datastore.insert(process_element(elem))

def process_element(elem):
    pelem = dict()
    if len(elem) > 0:
        pelem[elem.tag] = {}
        for e in elem:
            # So, this isn't perfect... it assumes you always have unique
            # elements...
            pelem[elem.tag].update(process_element(e))
    else:
        pelem[elem.tag] = elem.text
    return pelem

context = etree.iterparse(XML_FILE, tag=TAG)
fast_iter(context,sloth_loves_chunk)
