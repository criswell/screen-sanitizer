#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   Abstract data file reader

   :author: Sam Hart <sam@glencoesoftware.com>

   Copyright (C) 2013 Glencoe Software, Inc. All rights reserved.
   Use is subject to license terms supplied in LICENSE.txt
"""

import csv, codecs
from openpyxl import load_workbook
from utils import get_proper_position

class UTF8Recoder(object):
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")

class UnicodeReader(object):
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self

class Position(object):
    def __init__(self, row_num=None, col_num=None, row_name=None, col_name=None):
        self.row = dict()
        self.col = dict()
        if row_name:
            self.row['name'] = row_name
        if row_num:
            self.row['num'] = row_num
        if col_name:
            self.col['name'] = col_name
        if col_num:
            self.col['num'] = col_num

class Cell(object):
    def __init__(self, value, pos):
        self.value = value
        self.pos = pos
        self.worksheet = ''

    def __repr__(self):
        return self.value

def _xls_reader(filename):
    wb = load_workbook(filename = filename, use_iterators = True)

    for ws in wb.worksheets:
        for r in ws.iter_rows():
            row = []
            for c in r:
                p = get_proper_position(c.coordinate)
                pos = Position(row_name = p[0], col_name=p[1], col_num=[1])
                cell = Cell(c.internal_value, pos)
                cell.worksheet = ws.title
                row.append(cell)
            yield row

def _csv_reader(filename):
    with open(filename, 'rb') as csvin:
        row_num = 1
        for r in UnicodeReader(csvin):
            row = []
            cel_num = 1
            for c in r:
                pos = Position(row_num = row_num, col_num = cel_num)
                cell = Cell(c, pos)
                cell.worksheet=filename
                row.append(cell)
                cel_num = cel_num + 1
            row_num = row_num + 1
            yield row

def _tsv_reader(filename):
    with open(filename, 'r') as tsvin:
        row_num = 1
        for rawline in tsvin.readlines():
            line = rawline.rstrip('\n')
            if not line.startswith('#') and len(line)>0:
                row = []
                col_num = 1
                ls = line.split('\t')
                for c in ls:
                    pos = Position(row_num = row_num, col_num = col_num)
                    cell = Cell(c, pos)
                    cell.worksheet = filename
                    row.append(cell)
                    col_num = col_num + 1
                row_num = row_num + 1
                yield row

def get_reader(type='csv'):
    """
    Reader iterator.

    filename is the filename of the file to read.
    type is the type of the file, one of 'csv', 'xls', 'tsv'
    """
    lreader = _xls_reader
    if type == 'csv':
        lreader = _csv_reader
    elif type == 'tsv':
        lreader = _tsv_reader
    
    return lreader
